package Task4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Organizer {

    private Organizer() {
    }

    /** Переводит документы из ArrayList в HashMap */
    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        Map<Integer, Document> mapDokuments = new HashMap<>();

        for (int i = 0; i < documents.size(); i++) {
            mapDokuments.put(documents.get(i).getId(), documents.get(i));
        }

        return mapDokuments;
    }
}
