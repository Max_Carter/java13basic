package Task4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {

        // Создаем документы
        ArrayList<Document> documents = new ArrayList<>();
        documents.add(new Document(123, "Biology", 258));
        documents.add(new Document(456, "Chemistry", 199));
        documents.add(new Document(789, "Physics", 219));
        documents.add(new Document(012, "Astronomy", 158));

        // Создаем Map и присваиваем ключи со значениями
        Map<Integer, Document> mapDokuments = new HashMap<>();
        mapDokuments = Organizer.organizeDocuments(documents);

        // Проверяем
        for (Map.Entry<Integer, Document> entry: mapDokuments.entrySet()) {
            System.out.println("ID: " + entry.getKey() + "; Name: " + entry.getValue().getName() + "; Pages: "
            + entry.getValue().getPageCount() + ".");
        }

        System.out.println(mapDokuments.entrySet());
    }
}
