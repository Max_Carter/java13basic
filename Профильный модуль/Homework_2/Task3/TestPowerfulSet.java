package Task3;

import java.util.HashSet;
import java.util.Set;

public class TestPowerfulSet {
    public static void main(String[] args) {

        // Создаем первй набор
        Set<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);

        // Создаем второй набор
        Set<Integer> set2 = new HashSet<>();
        set2.add(0);
        set2.add(1);
        set2.add(2);
        set2.add(4);

        // Выводим результат
        System.out.println(PowerfulSet.intersection(set1, set2));
        System.out.println(PowerfulSet.union(set1, set2));
        System.out.println(PowerfulSet.relativeComplement(set1, set2));
    }
}
