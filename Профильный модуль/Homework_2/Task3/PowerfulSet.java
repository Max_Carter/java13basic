package Task3;

import java.util.HashSet;
import java.util.Set;

public class PowerfulSet {

    private PowerfulSet() {
    }

    /** Возвращает пересечение двух наборов */
    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> newSet = new HashSet<>();

        newSet.addAll(set1);
        newSet.retainAll(set2);

        return newSet;
    }

    /** Возвращает объединение двух наборов */
    public static <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> newSet = new HashSet<>();

        newSet.addAll(set1);
        newSet.addAll(set2);

        return newSet;
    }

    /** Возвращает элементы первого набора без тех, которые
     *  находятся такжеи во втором наборе */
    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        Set<T> newSet = new HashSet<>();

        for (T element: set1) {
            if (!set2.contains(element)) {
                newSet.add(element);
            }
        }

        return newSet;
    }
}
