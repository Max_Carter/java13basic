import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Task1 {
    public static void main(String[] args) {

        // Создаем массив строк
        ArrayList<String> list1 = new ArrayList<>();
        list1.add("one");
        list1.add("two");
        list1.add("three");

        // Создаем массив чисел типа Integer
        ArrayList<Integer> list2 = new ArrayList<>();
        list2.add(1);
        list2.add(2);
        list2.add(3);

        // Создаем массив чисел типа Double
        ArrayList<Double> list3 = new ArrayList<>();
        list3.add(1.0);
        list3.add(2.0);
        list3.add(3.0);

        // Выводим содержимое массивов
        System.out.println(method(list1));
        System.out.println(method(list2));
        System.out.println(method(list3));
    }

    /**
     * Возвращает набор уникальных элементов массива
     */
    public static <T> Set<T> method(ArrayList<T> list) {
        Set<T> set = new HashSet<>();

        for (T element: list) {
            set.add(element);
        }
        return set;
    }
}
