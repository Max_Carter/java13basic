import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Task2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        // Вводим два слова
        System.out.println("Введите первое слово: ");
        String s = input.nextLine();
        System.out.println("Введите второе слово: ");
        String t = input.nextLine();

        // Создаем два сета
        Set<Character> set1 = new HashSet<>();
        for (int i = 0; i < s.length(); i++) {
            set1.add(s.charAt(i));
        }
        Set<Character> set2 = new HashSet<>();
        for (int i = 0; i < t.length(); i++) {
            set2.add(t.charAt(i));
        }

        // Результат сравнения двух сетов
        System.out.println("Являются ли строки валидной анаграммой: " + isEquals(set1, set2));
    }

    /**
     * Сравнивает два сета
     */
    public static boolean isEquals(Set<Character> set1, Set<Character> set2) {
        if (set1 == null || set2 == null)
            return false;

        if (set1.size() != set2.size())
            return false;

        return set1.containsAll(set2);
    }
}
