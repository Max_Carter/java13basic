package Task3;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        String inputFileLocation = "C:\\Users\\user\\IdeaProjects\\DZprof_1\\src\\Task3\\file";

        try {
            ReadAndWriteFile.readAndWriteData(inputFileLocation);
        }
        catch (IOException ex) {
            System.out.println(ex);
        }

        // без параметра
        try {
            ReadAndWriteFile.readAndWriteData();
        }
        catch (IOException ex) {
            System.out.println(ex);
        }
    }
}
