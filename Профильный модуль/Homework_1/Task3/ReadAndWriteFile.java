package Task3;

import java.io.*;
import java.util.Scanner;

public class ReadAndWriteFile {

    private static final String FOLDER_DIRECTORY = "C:\\Users\\user\\IdeaProjects\\DZprof_1\\src\\Task3\\file";

    private static final String OUTPUT_FILE_NAME = "output.txt";

    private ReadAndWriteFile() {
    }

    public static void readAndWriteData(String filePath) throws IOException {
        Scanner scanner = new Scanner(new File(filePath));

        String[] letters = new String[30];
        int i = 0;
        while (scanner.hasNextLine()) {
            letters[i++] = scanner.nextLine();
        }

        Writer writer = new FileWriter(FOLDER_DIRECTORY + "\\" + OUTPUT_FILE_NAME);
        for (int j = 0; j < i; j++) {
            String res = Letters.ofLetter(letters[j]) + "\n";
            writer.write(res);
        }

        writer.close();
        scanner.close();
    }

    public static void readAndWriteData() throws IOException {
        readAndWriteData(FOLDER_DIRECTORY + "\\input.txt");
    }
}
