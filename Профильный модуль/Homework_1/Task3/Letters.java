package Task3;

public enum Letters {
    A("A"),
    B("B"),
    C("C"),
    D("D"),
    E("E"),
    F("F"),
    G("G"),
    H("H"),
    I("I"),
    J("J"),
    K("K"),
    L("L"),
    M("M"),
    N("N"),
    O("O"),
    P("P"),
    Q("Q"),
    R("R"),
    S("S"),
    T("T"),
    U("U"),
    V("V"),
    W("W"),
    X("X"),
    Y("Y"),
    Z("Z"),
    NOT_A_LETTER("Это не буква");

    public final String upperLetter;

    private static final Letters[] ALL = values();

    Letters(String upperLetter) {
        this.upperLetter = upperLetter;
    }

    public static Letters ofLetter(String letter) {
        for (Letters upLet : ALL) {
            if (upLet.upperLetter.toLowerCase().equals(letter))
                return upLet;
        }
        return NOT_A_LETTER;
    }

}
