package Task1;

import java.util.Scanner;

public class MyCheckedExceptionTest {
    public static void main(String[] args) {
        int side;
        Scanner input = new Scanner(System.in);

        // Вводим сторону квадрата
        System.out.println("Введите сторону квадрата: ");
        side = input.nextInt();

        try {
            Square square = new Square(side);
            System.out.println("Стороны квадрата равны: " + square.getSide());
        }
        catch (MyCheckedException ex) {
            System.out.println(ex);
        }
    }
}
