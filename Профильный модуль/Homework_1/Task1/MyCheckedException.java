package Task1;

public class MyCheckedException extends Exception {
    private int side;

    // Создает исключение
    public MyCheckedException(int side) {
        super("Недопустимое значение: " + side);
        this.side = side;
    }
}
