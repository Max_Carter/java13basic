package Task1;

public class Square {
    private int side;

    // Создает квадрат с указанной стороной
    public Square(int newSide) throws MyCheckedException {
        setSide(newSide);
    }

    // Возвращает сторону квадрата
    public int getSide() {
        return side;
    }

    // Присваивает новую сторону
    public void setSide(int newSide) throws MyCheckedException {
        if (newSide > 0)
            side = newSide;
        else
            throw new MyCheckedException(newSide);
    }
}
