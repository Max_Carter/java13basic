package Task5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int n = 0;
        try {
            inputN(n);
            System.out.println("Успешный ввод!");
        }
        catch (Exception ex) {
            System.out.println(ex);
        }
    }
    public static int inputN(int n) throws Exception{
        System.out.println("Введите число n, 0 < n < 100");
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();
        if (n > 100 || n < 0) {
            throw new Exception("Неверный ввод");
        }
        return n;
    }

}
