package Task4;

import java.util.Scanner;

public class MyEvenNumberTest {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n;

        System.out.println("Введите четное число: ");
        n = input.nextInt();

        try {
            MyEvenNumber number = new MyEvenNumber(n);
            System.out.println("Введенное четное число: " + number.getEvenNumber());
        }
        catch (NumberIsNotEvenException ex) {
            System.out.println(ex);
        }
    }
}
