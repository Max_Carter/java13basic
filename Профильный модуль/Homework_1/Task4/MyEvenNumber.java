package Task4;

public class MyEvenNumber {
    private int evenNumber;

    public MyEvenNumber(int evenNumber) throws NumberIsNotEvenException {
        setEvenNumber(evenNumber);
    }

    public int getEvenNumber() {
        return evenNumber;
    }

    public void setEvenNumber(int newEvenNumber) throws NumberIsNotEvenException {
        if (newEvenNumber % 2 == 0)
            evenNumber = newEvenNumber;
        else
            throw new NumberIsNotEvenException(newEvenNumber);
    }
}
