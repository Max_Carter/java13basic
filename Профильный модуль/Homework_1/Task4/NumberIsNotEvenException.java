package Task4;

public class NumberIsNotEvenException extends Exception {
    private int notEvenNumber;

    public NumberIsNotEvenException (int notEvenNumber) {
        super("Число нечетное!");
        this.notEvenNumber = notEvenNumber;
    }
}
