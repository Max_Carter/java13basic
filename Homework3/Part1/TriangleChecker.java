public class TriangleChecker {
    public static int a;
    public static int b;
    public static int c;
    public static boolean isFeasible;

    public TriangleChecker(int a, int b, int c) {
      this.a = a;
      this.b = b;
      this.c = c;
    }

    public static boolean isFeasibleTriangle() {
        if (a + b > c)
            if (a + c > b)
                if (b + c > a)
                    isFeasible = true;
        else
            isFeasible = false;
        return isFeasible;
    }
}
