public class Atm {
    public double roublesPerDollar;
    public double dollarsPerRouble;
    public double dollars;
    public double roubles;
    private static int countOfInstance;

    public Atm(double roublesPerDollar, double dollarsPerRouble) {
        this.roublesPerDollar = roublesPerDollar;
        this.dollarsPerRouble = dollarsPerRouble;
    }

    public double convertDollarsToRoubles(double dollars) {
        roubles = roublesPerDollar * dollars;
        countOfInstance++;
        return roubles;
    }

    public double convertRoublesToDollars(double roubles) {
        dollars = dollarsPerRouble * roubles;
        countOfInstance++;
        return dollars;
    }

    public static int getCountOfInstance() {
        return countOfInstance;
    }

}
