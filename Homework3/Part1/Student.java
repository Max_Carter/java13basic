public class Student {
    private String name;
    private String surname;
    private int[] grades = new int[10];
    private static int numberOfGrades;

    public Student() {

    }

    public Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    public void addGrade(int grade) {
        if (numberOfGrades < 10) {
            grades[numberOfGrades] = grade;
            numberOfGrades++;
        } else {
            for (int i = 1; i < 10; i++) {
                grades[i - 1] = grades[i];
            }
            grades[9] = grade;
        }
    }

    public double getGPA(int[] grades) {
        double gpa;
        double total = 0;
        for (int i = 0; i < 10; i++)
            total += grades[i];
        gpa = total / 10;
        return gpa;
    }
}
