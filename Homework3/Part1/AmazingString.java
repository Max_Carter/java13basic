public class AmazingString {
    public char[] chars;
    public boolean isFound;

    public AmazingString(char[] ch) {
        chars = new char[ch.length];
        System.arraycopy(ch, 0, chars, 0, ch.length);
    }

    public AmazingString(String str) {
        chars = new char[str.length()];
        for (int i = 0; i < chars.length; i++)
            chars[i] = str.charAt(i);
    }

    public char getChar(int i) {
        return chars[i - 1];
    }

    public int getLength() {
        return chars.length;
    }

    public String printString() {
        String newString = String.copyValueOf(chars);
        return newString;
    }

    public boolean checkSubstring(char[] ch) {
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == ch[0]) {
                char[] newCh = new char[ch.length];
                for (int k = 0; k < ch.length; k++) {
                    newCh[k] = chars[i];
                    i++;
                }
                for (int j = 0; j < ch.length; j++) {
                    if (newCh[j] == ch[j])
                        isFound = true;
                    else {
                        isFound = false;
                        break;
                    }
                }
            }
        }
        return isFound;
    }

    public boolean checkSubstring(String str) {
        char[] ch = new char[str.length()];
        for (int n = 0; n < ch.length; n++)
            ch[n] = str.charAt(n);

        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == ch[0]) {
                char[] newCh = new char[ch.length];
                System.arraycopy(chars, i, newCh, 0, ch.length);
                for (int j = 0; j < ch.length; j++) {
                    if (newCh[j] == ch[j])
                        isFound = true;
                    else {
                        isFound = false;
                        break;
                    }
                }
            }
        }
        return isFound;
    }

    public String reverseString() {
        char[] ch = new char[chars.length];
        int i = 0;
        for (int j = chars.length - 1; j >= 0; j--) {
            ch[i] = chars[j];
            i++;
        }
        String newString = String.copyValueOf(ch);
        return newString;
    }

    public String deleteScpaces() {
        int countOfSpaces = 0;
        for (int i = 0; i < chars.length; i++) {
            if (Character.isWhitespace(chars[i])) {
                countOfSpaces++;
            } else
                break;
        }
        String newString2 = String.copyValueOf(chars);
        if (countOfSpaces > 0) {
            char[] newCh = new char[chars.length - countOfSpaces];
            System.arraycopy(chars, countOfSpaces, newCh, 0, newCh.length);
            String newString1 = String.copyValueOf(newCh);
            return newString1;
        } else
            return newString2;
    }
}
