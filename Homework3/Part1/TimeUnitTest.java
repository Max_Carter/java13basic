import java.util.Calendar;
import java.util.Scanner;

public class TimeUnitTest {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Calendar calendar = Calendar.getInstance();
        int hours;
        int minutes;
        int seconds;

        System.out.println("Введите количество часов: ");
        hours = input.nextInt();
        System.out.println("Введите количество минут, либо нажмите Enter: ");
        minutes = input.nextInt();
        System.out.println("Введите количество секунд, либо нажмите Enter: ");
        seconds = input.nextInt();

        if (hours > 0) {
            if (seconds == 0 && minutes == 0) {
                TimeUnit time = new TimeUnit(hours);
            } else if (seconds == 0 && minutes > 0) {
                TimeUnit time = new TimeUnit(hours, minutes);
            } else if (seconds > 0 && minutes > 0) {
                TimeUnit time = new TimeUnit(hours, minutes, seconds);
            }
        }

        calendar = TimeUnit.get24HourFormat();
        System.out.println(calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE)
                + ":" + calendar.get(Calendar.SECOND));

        calendar = TimeUnit.get12HourFormat();
        System.out.print(calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE)
                + ":" + calendar.get(Calendar.SECOND) + " ");
        if (calendar.get(Calendar.AM_PM) == 0)
            System.out.println("PM");
        else
            System.out.println("AM");

        calendar = TimeUnit.increaseTime24();
        System.out.println(calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE)
                + ":" + calendar.get(Calendar.SECOND));

        calendar = TimeUnit.increaseTime12();
        System.out.print(calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE)
                + ":" + calendar.get(Calendar.SECOND) + " ");
        if (calendar.get(Calendar.AM_PM) == 0)
            System.out.println("PM");
        else
            System.out.println("AM");
    }
}
