public class StudentTest {
    public static void main(String[] args) {
        Student student1 = new Student("Алена", "Кемова");
        Student student2 = new Student("Глеб", "Варганов");
        Student student3 = new Student("Анастасия", "Ельшина");
        Student student4 = new Student("Ангелина", "Бондаренко");
        StudentService studentService = new StudentService();

        studentService.addStudent(student1);
        studentService.addStudent(student2);
        studentService.addStudent(student3);
        studentService.addStudent(student4);


        Student[] UnsortedStudentsList = studentService.getStudentsList();
        Student[] studentsList = studentService.sortBySurname(UnsortedStudentsList);

        System.out.print("Студенты: ");
        for (int i = 0; i < studentsList.length; i++)
            if (i < studentsList.length - 1)
                System.out.print(studentsList[i].getSurname() + " " + studentsList[i].getName() + ", ");
            else {
                System.out.print(studentsList[i].getSurname() + " " + studentsList[i].getName() + ".");
                System.out.println();
            }

        System.out.println("-----------------------------------------------");

        for (int i = 0; i < 10; i++)
            student1.addGrade((int) (Math.random() * 4) + 2);

        for (int i = 0; i < 10; i++)
            student2.addGrade((int) (Math.random() * 4) + 2);

        for (int i = 0; i < 10; i++)
            student3.addGrade((int) (Math.random() * 4) + 2);

        for (int i = 0; i < 10; i++)
            student4.addGrade((int) (Math.random() * 4) + 2);

        System.out.println("Студент " + student1.getName() + " " + student1.getSurname() + " имеет оценки:");
        int[] grades1 = new int[10];
        grades1 = student1.getGrades();
        for (int i = 0; i < 10; i++) {
            if (i < 9)
                System.out.print(grades1[i] + ", ");
            else {
                System.out.print(grades1[i] + ".");
                System.out.println();
            }
        }

        System.out.println("Студент " + student2.getName() + " " + student2.getSurname() + " имеет оценки:");
        int[] grades2 = new int[10];
        grades2 = student2.getGrades();
        for (int i = 0; i < 10; i++) {
            if (i < 9)
                System.out.print(grades2[i] + ", ");
            else {
                System.out.print(grades2[i] + ".");
                System.out.println();
            }
        }

        System.out.println("Студент " + student3.getName() + " " + student3.getSurname() + " имеет оценки:");
        int[] grades3 = new int[10];
        grades3 = student3.getGrades();
        for (int i = 0; i < 10; i++) {
            if (i < 9)
                System.out.print(grades3[i] + ", ");
            else {
                System.out.print(grades3[i] + ".");
                System.out.println();
            }
        }

        System.out.println("Студент " + student4.getName() + " " + student4.getSurname() + " имеет оценки:");
        int[] grades4 = new int[10];
        grades4 = student4.getGrades();
        for (int i = 0; i < 10; i++) {
            if (i < 9)
                System.out.print(grades4[i] + ", ");
            else {
                System.out.print(grades4[i] + ".");
                System.out.println();
            }
        }

        System.out.println("-----------------------------------------------");

        System.out.println("Средний балл студента " + student1.getName() + " "
                + student1.getSurname() + ": " + student1.getGPA(grades1));

        System.out.println("Средний балл студента " + student2.getName() + " "
                + student2.getSurname() + ": " + student2.getGPA(grades2));

        System.out.println("Средний балл студента " + student3.getName() + " "
                + student3.getSurname() + ": " + student3.getGPA(grades3));

        System.out.println("Средний балл студента " + student4.getName() + " "
                + student4.getSurname() + ": " + student4.getGPA(grades4));

        System.out.println("-----------------------------------------------");

        System.out.println("Студент, имеющий самый высокий средний балл: " +
                studentService.bestStudent(studentsList).getName() + " " +
                studentService.bestStudent(studentsList).getSurname());

    }
}
