import java.util.Calendar;

public class TimeUnit {
    private static int hours;
    private static int minutes;
    private static int seconds;

    public TimeUnit(int hours, int minutes, int seconds) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    public TimeUnit(int hours, int minutes) {
        this.hours = hours;
        this.minutes = minutes;
        this.seconds = 0;
    }

    public TimeUnit(int hours) {
        this.hours = hours;
        this.minutes = 0;
        this.seconds = 0;
    }

    public static Calendar get24HourFormat() {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, hours);
        calendar.set(Calendar.MINUTE, minutes);
        calendar.set(Calendar.SECOND, seconds);

        return calendar;
    }

    public static Calendar get12HourFormat() {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR, hours);
        calendar.set(Calendar.MINUTE, minutes);
        calendar.set(Calendar.SECOND, seconds);

        return calendar;
    }

    public static Calendar increaseTime24() {
        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.HOUR_OF_DAY, hours);
        calendar.add(Calendar.MINUTE, minutes);
        calendar.add(Calendar.SECOND, seconds);

        return calendar;
    }

    public static Calendar increaseTime12() {
        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.HOUR, hours);
        calendar.add(Calendar.MINUTE, minutes);
        calendar.add(Calendar.SECOND, seconds);

        return calendar;
    }
}
