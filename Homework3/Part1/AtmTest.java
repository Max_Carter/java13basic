import java.util.Scanner;

public class AtmTest {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите курс валют перевода долларов в рубли и курс валют перевода рублей в доллары:");
        Atm atm = new Atm(input.nextDouble(), input.nextDouble());

        System.out.print("Введите сумму в долларах: ");
        double dollarsToRoubles1 = atm.convertDollarsToRoubles(input.nextDouble());
        System.out.println(dollarsToRoubles1);
        System.out.print("Введите сумму в долларах: ");
        double dollarsToRoubles2 = atm.convertDollarsToRoubles(input.nextDouble());
        System.out.println(dollarsToRoubles2);
        System.out.print("Введите сумму в долларах: ");
        double dollarsToRoubles3 = atm.convertDollarsToRoubles(input.nextDouble());
        System.out.println(dollarsToRoubles3);

        System.out.print("Введите сумму в рублях: ");
        double roublesToDollars1 = atm.convertRoublesToDollars(input.nextDouble());
        System.out.println(roublesToDollars1);
        System.out.print("Введите сумму в рублях: ");
        double roublesToDollars2 = atm.convertRoublesToDollars(input.nextDouble());
        System.out.println(roublesToDollars2);

        System.out.print("Количество созданных инстансов класса Atm: " + Atm.getCountOfInstance());
    }
}
