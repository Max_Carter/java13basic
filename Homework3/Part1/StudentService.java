import java.util.Arrays;

public class StudentService {
    public Student[] studentsList = new Student[10];
    private static int numberOfStudents = 0;

    public void addStudent(Student student) {
        studentsList[numberOfStudents] = student;
        numberOfStudents++;
    }

    public Student[] getStudentsList() {
        Student[] result = new Student[numberOfStudents];
        for (int i = 0; i < numberOfStudents; i++)
            result[i] = studentsList[i];
        return result;
    }

    public Student bestStudent(Student[] studentsList) {
        double maxGrades = 0;
        int studentsId = -1;
        for (int i = 0; i < studentsList.length; i++) {
            int[] grades = new int[10];
            grades = studentsList[i].getGrades();
            if (studentsList[i].getGPA(grades) > maxGrades) {
                maxGrades = studentsList[i].getGPA(studentsList[i].getGrades());
                studentsId = i;
           }
        }
        return studentsList[studentsId];
    }

    public Student[] sortBySurname(Student[] studentsList) {
        Student[] temp = new Student[1];
        for (int i = 0; i < studentsList.length - 1; i++) {
            String currentMin = studentsList[i].getSurname();
            int currentMinIndex = i;
            for (int j = i + 1; j < studentsList.length; j++) {
                if (currentMin.compareTo(studentsList[j].getSurname()) > 0) {
                    currentMin = studentsList[j].getSurname();
                    currentMinIndex = j;
                }
            }
            if (currentMinIndex != i) {
                System.arraycopy(studentsList, currentMinIndex, temp, 0, temp.length);
                studentsList[currentMinIndex] = studentsList[i];
                studentsList[i] = temp[0];
            }
        }
        return studentsList;
    }



}
