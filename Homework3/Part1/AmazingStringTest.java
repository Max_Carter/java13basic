import java.util.Scanner;

public class AmazingStringTest {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char[] arrayChars = new char[6];

        System.out.println("Введите 6 символов через пробел: ");
        for (int i = 0; i < arrayChars.length; i++){
            String s = input.next();
            arrayChars[i] = s.charAt(0);
        }
        AmazingString amazingString1 = new AmazingString(arrayChars);

        System.out.println("Введите строку символов: ");
        String string = input.next();
        AmazingString amazingString2 = new AmazingString(string);

        System.out.println("Введите номер символа, который нужно вернуть: ");
        int index = input.nextInt();
        System.out.println("Индекс amazingString1: " + amazingString1.getChar(index));
        System.out.println("Индекс amazingString2: " + amazingString2.getChar(index));

        System.out.println();
        System.out.println("Длина amazingString1: " + amazingString1.getLength());
        System.out.println("Длина amazingString2: " + amazingString2.getLength());

        System.out.println();
        System.out.println("Строка amazingString1: " + amazingString1.printString());
        System.out.println("Строка amazingString2: " + amazingString2.printString());

        System.out.println();
        System.out.println("Введите 3 символа подстроки, которые нужно найти:");
        char[] ch = new char[3];
        for (int i = 0; i < ch.length; i++){
            String s = input.next();
            ch[i] = s.charAt(0);
        }
        System.out.println("Содержание подстроки в amazingString1: " + amazingString1.checkSubstring(ch));

        System.out.println();
        System.out.println("Введите подстроку, которую нужно найти:");
        String str = input.next();
        System.out.println("Содержание подстроки в amazingString2: " + amazingString2.checkSubstring(str));

        System.out.println("Строка amazingString1 с удаленными ведущими пробельными символами: "
                + amazingString1.deleteScpaces());
        System.out.println("Строка amazingString2 с удаленными ведущими пробельными символами: "
                + amazingString2.deleteScpaces());

        System.out.println("Развернутая строка amazingString1: " + amazingString1.reverseString());
        System.out.println("Развернутая строка amazingString2: " + amazingString2.reverseString());
    }
}
