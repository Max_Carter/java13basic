public class DayOfWeek {
    private byte num;
    private String week;

    public DayOfWeek(byte num, String week) {
        this.num = num;
        this.week = week;
    }

    public byte getNum() {
        return num;
    }

    public String getWeek() {
        return week;
    }
}
