public class Cat {
    private String sleep = "Sleep";
    private String meow = "Meow";
    private String eat = "Eat";

    public Cat() {
    }
    
    private String getSleep() {
        return sleep;
    }
    
    private String getMeow() {
        return meow;
    }
    
    private String getEat() {
        return eat;
    }

    public String status() {
        String s = null;
        int i = (int)(Math.random() * 3);
        if (i == 0)
             s = getSleep();
        if (i == 1)
            s = getMeow();
        if (i == 2)
            s = getEat();
        return s;
    }

}
