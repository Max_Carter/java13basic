public class DayOfWeekTest {
    public static void main(String[] args) {
        DayOfWeek[] weeks = new DayOfWeek[7];
        weeks[0] = new DayOfWeek((byte) 1, "Monday");
        weeks[1] = new DayOfWeek((byte) 2, "Tuesday");
        weeks[2] = new DayOfWeek((byte) 3, "Wednesday");
        weeks[3] = new DayOfWeek((byte) 4, "Thursday");
        weeks[4] = new DayOfWeek((byte) 5, "Friday");
        weeks[5] = new DayOfWeek((byte) 6, "Saturday");
        weeks[6] = new DayOfWeek((byte) 7, "Sunday");

        for (int i = 0 ; i < weeks.length; i++) {
            System.out.println(weeks[i].getNum() + " " + weeks[i].getWeek());
        }
    }
}
