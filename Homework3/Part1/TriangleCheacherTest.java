import java.util.Scanner;

public class TriangleCheacherTest {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Введите три стороны треугольника:");
        int a = input.nextInt();
        int b = input.nextInt();
        int c = input.nextInt();
        TriangleChecker triangleChecker = new TriangleChecker(a, b, c);
        System.out.println(triangleChecker.isFeasibleTriangle());
    }
}
