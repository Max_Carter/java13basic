package Task1;

public class AnimalTest {
    public static void main(String[] args) {
        Bat bat = new Bat();
        Eagle eagle = new Eagle();
        Dolphin dolphin = new Dolphin();
        GoldFish goldFish = new GoldFish();

        bat.eat();
        bat.sleep();
        eagle.sleep();
        goldFish.eat();

        dolphin.wayOfBirth();
        eagle.wayOfBirth();
        goldFish.wayOfBirth();

        bat.speedOfFlying();
        eagle.speedOfFlying();
        dolphin.speedOfSwimming();
        goldFish.speedOfSwimming();
    }
}
