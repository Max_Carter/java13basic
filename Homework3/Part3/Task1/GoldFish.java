package Task1;

public class GoldFish extends Fish implements Swimming {

    public GoldFish() {
    }

    public void speedOfSwimming() {
        System.out.println("Золотая рыбка плавает медленно");
    }
}
