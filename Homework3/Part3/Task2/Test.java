package Task2;

public class Test {
    public static void main(String[] args) {
        Stool stool = new Stool();
        Table table = new Table();

        if (stool.isFixable(stool))
            System.out.println("Можно починить");
        else
            System.out.println("Нельзя починить");

        if (stool.isFixable(table))
            System.out.println("Можно починить");
        else
            System.out.println("Нельзя починить");
    }
}
