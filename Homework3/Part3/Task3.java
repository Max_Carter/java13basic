import java.util.ArrayList;
import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n; // столбец
        int m; // строка

        System.out.println("Введите столбец и строку массива через пробел: ");
        n = input.nextInt();
        m = input.nextInt();

        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();
        ArrayList<Integer> list;

        for (int i = 0; i < m; i++) {
            list = new ArrayList<>();
            for (int j = 0; j < n; j++) {
                list.add(i + j);
            }
            matrix.add(list);
        }

        System.out.println(matrix);
    }
}
