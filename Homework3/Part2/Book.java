import java.util.ArrayList;

public class Book {
    private String author;
    private String title;
    private Visitor whoTake = null;

    public Book(){
    }

    public Book(String author, String title) {
        this.author = author;
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }
}
