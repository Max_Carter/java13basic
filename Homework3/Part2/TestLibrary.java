import java.util.Scanner;
import java.util.ArrayList;

public class TestLibrary {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Book book1 = new Book("А. Стругацкий, Б. Стругацкий", "Улитка на склоне");
        Book book2 = new Book("А. Стругацкий, Б. Стругацкий", "Понедельник начинается в субботу");
        Book book3 = new Book("А. Стругацкий, Б. Стругацкий", "Сказка о тройке");
        Book book4 = new Book("А. Стругацкий, Б. Стругацкий", "Второе нашествие марсиан");
        Book book5 = new Book("Нил Гейман", "Коралина");
        Book book6 = new Book("Нил Гейман", "Звездная пыль");
        Book book7 = new Book("Агата Кристи", "Десять нигретят");
        Book book8 = new Book("Агата Кристи", "Загадка Эндхауза");
        Book book9 = new Book("Сергей Лукьяненко", "Фальшивые зеркала");
        Book book10 = new Book("Маркус Зусак", "Книжный вор");

        Library library = new Library();
        library.bookList.add(book1);
        library.bookList.add(book2);
        library.bookList.add(book3);
        library.bookList.add(book4);
        library.bookList.add(book5);
        library.bookList.add(book6);
        library.bookList.add(book7);
        library.bookList.add(book8);
        library.bookList.add(book9);
        library.bookList.add(book10);

        library.printListOfTitles();

        if (library.addNewBook(book2))
            System.out.println("Книга добавлена.");
        if (library.addNewBook(new Book("А. Стругацкий, Б. Стругацкий", "Повесть о дружбе и недружбе")))
            System.out.println("Книга добавлена.");

        library.printListOfTitles();

        library.deleteBook(new Book("Жюль Верн", "Дети капитана Гранта"));
        library.deleteBook(book9);

        library.printListOfTitles();

        Book book11 = new Book();
        book11 = library.findBook("Книжный вор").get(0);
        library.printTitle(book11);

        library.printListOfAuthor(library.findAuthor("А. Стругацкий, Б. Стругацкий"));
        library.printListOfAuthor(library.findAuthor("Агата Кристи"));
    }
}
