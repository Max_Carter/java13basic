import java.util.ArrayList;

public class Library {
    public static ArrayList<Book> bookList = new ArrayList<>();

    public Library() {
    }

    public boolean addNewBook(Book book) {
        if (!book.getTitle().isEmpty() && !book.getAuthor().isEmpty()) {
            if (!bookList.contains(book)) {
                bookList.add(book);
                return true;
            } else {
                System.out.println("Книга \"" + book.getTitle() + "\" уже имеется в библиотеке.");
                return false;
            }
        } else {
            System.out.println("Не заполнены обязательные поля книги.");
            return false;
        }
    }

    public void deleteBook(Book book) {
        if (bookList.contains(book)) {
            bookList.remove(book);
            System.out.println("Книга \"" + book.getTitle() + "\" удалена.");
        }
        else
            System.out.println("Книги \"" + book.getTitle() + "\" нет в библиотеке.");
    }

    public ArrayList<Book> findBook(String t) {
        int indexOfBook = -1;
        for (int i = 0; i < bookList.size(); i++) {
            if ((bookList.get(i)).getTitle().equals(t)) {
                indexOfBook = i;
                break;
            }
        }
        ArrayList<Book> list = new ArrayList<>();
        list.add(bookList.get(indexOfBook));
        return list;
    }

    public ArrayList<Book> findAuthor(String a) {
        ArrayList<Book> list = new ArrayList<>();
        int indexOfBook = -1;
        for (int i = 0; i < bookList.size(); i++) {
            if ((bookList.get(i)).getAuthor().equals(a)) {
                indexOfBook = i;
                list.add(bookList.get(indexOfBook));
            }
        }
        System.out.println("Книги автора(-ов) " + a + " найдены: ");
        return list;
    }

    public void printListOfTitles() {
        System.out.print("Книги в библиотеке: ");
        for (int i = 0; i < bookList.size(); i++)
            System.out.print("\"" + bookList.get(i).getTitle() + "\"," + " ");
        System.out.println();
    }

    public void printTitle(Book book) {
        System.out.println("Книга " + book.getTitle() + " найдена.");
    }

    public void printListOfAuthor(ArrayList<Book> list) {
        for (int i = 0; i < list.size(); i++)
            System.out.print((list.get(i)).getAuthor() + " " + "\"" + (list.get(i)).getTitle() + "\", ");
        System.out.println();
    }

}
