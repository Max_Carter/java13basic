package part2;

import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int m = 1 + (int)(Math.random() * 1000);
        int n = input.nextInt();

        System.out.println(game(m, n));
    }

    public static String game(int m, int n) {
        Scanner input = new Scanner(System.in);
        if (n < 0)
            return "";
        if (n == m)
            return "Победа!";
        else if (n < m) {
            System.out.println("Это число меньше загаданного.");
            n = input.nextInt();
            return game(m, n);
        } else {
            System.out.println("Это число больше загаданного.");
            n = input.nextInt();
            return game(m, n);
        }
    }
}
